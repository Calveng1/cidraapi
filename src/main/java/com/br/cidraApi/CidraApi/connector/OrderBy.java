package com.br.cidraApi.CidraApi.connector;

import org.hibernate.criterion.Order;

/**
 *
 * @author jean
 */
public class OrderBy {
    private Order order;
    private String associationPath;

    public OrderBy(Order order){
        this(order, "this");
    }

    public OrderBy(Order order, String associationPath) {
        this.order = order;
        this.associationPath = associationPath;
    }

    /**
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     * @return the associationPath
     */
    public String getAssociationPath() {
        return associationPath;
    }

    /**
     * @param associationPath the associationPath to set
     */
    public void setAssociationPath(String associationPath) {
        this.associationPath = associationPath;
    }

    

}
