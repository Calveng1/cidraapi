package com.br.cidraApi.CidraApi.connector;

import org.hibernate.criterion.Projection;

/**
 * Classe auxiliar para geração de projections no padrão Parceria Generic.
 * 
 * @since 29/06/2017
 * @author ademilson
 */
public class ProjectionParc {
    
    private String alias;
    private Projection projection;

    
    public static ProjectionParc novo(Projection p){
        return new ProjectionParc(p);
    }
    
    /**
     * @param projection Este campo <b>é obrigatório</b> ser informado. 
     * <br/><b>Exemplo de uso:</b> Projections.avg("idade"), * Projections.property("ttDocFiscal"), Projections.sum("ttPrecoRealizado")...
     */
    public ProjectionParc(Projection projection) {
        this(null, projection);
    }
    
    /**
     * @param associationPath Esse campo é uma precaução. Pode ser definido o associationPath no setJoin do GenericDao. 
     * Mas se o campo estiver definido como MERGE no mapeamento normalmente não é declarado no setJoin, enão por isso que deve ser passado aqui.
     * <br/><b>Exemplo de uso:</b> Voce está com o controller docFiscalCon, e precisa do objeto municipio que está dentro do participante, então deve ser feito o seguinte:
     * participante.municipio.
     * <br/>
     * <br/>
     * @param projection Este campo <b>é obrigatório</b> ser informado. 
     * <br/><b>Exemplo de uso:</b> Projections.avg("idade"), * Projections.property("ttDocFiscal"), Projections.sum("ttPrecoRealizado")...
     */
    public ProjectionParc(String associationPath, Projection projection) {
        this.alias = alias;
        this.projection = projection;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Projection getProjection() {
        return projection;
    }

    public void setProjection(Projection projection) {
        this.projection = projection;
    }
}

