package com.br.cidraApi.CidraApi.connector;

//import br.tuaagenda.util.Util;
//import br.tuaagenda.util.Verify;
import com.br.cidraApi.CidraApi.util.Util;
import com.br.cidraApi.CidraApi.util.Verify;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ademilson
 */
@Repository
public class HibernateUtil {
    
    private static final SessionFactory sessionFactory;
    private static Properties properties;

    static {
        
        String profile = System.getProperty("spring.profiles.active");
        String applicationProperties = "/application.properties";
        
        if (Verify.equals(profile, "prod")) {
            applicationProperties = "/application-prod.properties";
        }

        Configuration annotationConfiguration = new Configuration().configure();
        properties = annotationConfiguration.getProperties();

        String production = "";
        
        try {
            Properties arquivoExterno = new Properties();
            //** Carrega arquivos de properties.
            arquivoExterno.load(new InputStreamReader(Util.class.getResourceAsStream(applicationProperties), StandardCharsets.UTF_8));

            production = arquivoExterno.getProperty("project.production");
        } catch (IOException ex) {
            System.err.println("Erro ao ler application.properties" + ex);
        }

        if (Verify.equals(production, "true")) {
            properties.setProperty("hibernate.show_sql", "false");
        }

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure().applySettings(properties).build();

        Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();

        sessionFactory = metadata.getSessionFactoryBuilder().build();

    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }

    public static void setProperty(String key, String value) {
        properties.setProperty(key, value);
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static Session openSession() {
        return getSessionFactory().openSession();
    }
}
