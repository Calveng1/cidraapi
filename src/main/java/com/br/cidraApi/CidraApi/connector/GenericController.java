package com.br.cidraApi.CidraApi.connector;

import org.hibernate.transform.ResultTransformer;


/**
 *
 * @author Ademilsom
 * @param <T>
 */
public class GenericController<T> extends GenericCON<T>{
    
    private Object[] bkp_criterions, bkp_defCriterions, bkp_joins;
    private OrderBy[] bkp_order, bkp_defOrder;
    private ProjectionParc[] bkp_projections;
    private ResultTransformer bkp_resultTransformer;
    private int bkp_maxFetchSize;
    
    public GenericController(Class classe) {
        this(classe, false);
    }
    
    public GenericController(Class classe, boolean considera) {
        super(classe, considera);
    }
    
    public boolean save(T object){
        setObjeto(object);
        alterar();
        return gravar();
    }
    
    public boolean delete(Long id){
        if(localizar(id))
            if(excluir()){
                return true;
            }
        
        return false;
    }
    
    public boolean delete(Integer id){
        if(localizar(id))
            if(excluir()){
                return true;
            }
        
        return false;
    }
    
    public void delete(T object){
        setObjeto(object);
        excluir();
    }
    
    public void guardaConfiguracoes() {
        bkp_joins = getJoin();
        bkp_criterions = getCriterions();
        bkp_defCriterions = getDefaultCriterions();
        bkp_order = getOrderByClause();
        bkp_defOrder = getDefaultOrderBy();
        bkp_projections = getProjections();
        bkp_resultTransformer = getResultTransformer();
        bkp_maxFetchSize = getMaxFetchSize();
        limpaConfiguracoes();
    }
    
    public void restauraConfiguracoes() {
        setJoin(bkp_joins);
        setCriterions(bkp_criterions);
        setDefaultCriterions(bkp_defCriterions);
        setOrderByClause(bkp_order);
        setDefaultOrderBy(bkp_defOrder);
        setProjections(bkp_projections);
        setResultTransformer(bkp_resultTransformer);
        setMaxFetchSize(bkp_maxFetchSize);
    }
    
    public void limpaConfiguracoes() {
        setJoin(null);
        setCriterions(null);
        setDefaultCriterions(null);
        setOrderByClause(null);
        setDefaultOrderBy(null);
        setProjections(null);
        setResultTransformer(null);
        setMaxFetchSize(-1);
    }
    
    
}
