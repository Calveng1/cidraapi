package com.br.cidraApi.CidraApi.controller;

import com.br.cidraApi.CidraApi.model.Userjson;
import com.br.cidraApi.CidraApi.repositorio.UserCon;
import com.br.cidraApi.CidraApi.security.Encrypter;
import com.br.cidraApi.CidraApi.util.Verify;
import java.security.NoSuchAlgorithmException;
import org.hibernate.criterion.Restrictions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Calven
 */
@Controller
@RequestMapping("/auth")
public class AuthController {

    private UserCon userCon = new UserCon();
    

    @PostMapping
    public ResponseEntity authUser(@RequestBody Userjson userJson) throws NoSuchAlgorithmException {
        
        Encrypter.encryptPassword(userJson);
        userCon.limpaConfiguracoes();
        userCon.setCriterions(new Object[]{
            new Object[]{"this", Restrictions.eq("username", userJson.getUsername())},
            new Object[]{"this", Restrictions.eq("password", userJson.getPassword())},
            new Object[]{"this", Restrictions.eq("stAtivo", "T")}});
        
        if (Verify.nullsOrEmpty(userCon.getUniqueResult())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(HttpStatus.UNAUTHORIZED);
        }
        return ResponseEntity.status(HttpStatus.OK).body(userCon.getUniqueResult());

    }

}
