package com.br.cidraApi.CidraApi.controller;

import com.br.cidraApi.CidraApi.connector.OrderBy;
import com.br.cidraApi.CidraApi.model.User;
import com.br.cidraApi.CidraApi.model.Userjson;
import com.br.cidraApi.CidraApi.repositorio.UserCon;
import com.br.cidraApi.CidraApi.security.Encrypter;
import com.br.cidraApi.CidraApi.util.Verify;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.validation.Valid;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Calven
 */
@Controller
@RequestMapping("/users")
public class UserController {

    private UserCon userCon = new UserCon();

    @PostMapping
    public ResponseEntity createUser(@Valid @RequestBody Userjson userJson) throws NoSuchAlgorithmException {
        
        User userComMesmoUsername = getUserByUsername(userJson.getUsername());
        if(!Verify.nullsOrEmpty(userComMesmoUsername)){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Nome de usuário não disponível.");
        }
        
        Encrypter.encryptPassword(userJson);
        if (userCon.addUser(userJson)) {
            return ResponseEntity.status(HttpStatus.CREATED).body(HttpStatus.CREATED);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/{idUser}")
    public ResponseEntity getUserById(@PathVariable Integer idUser) {

        userCon.limpaConfiguracoes();
        userCon.setCriterions(new Object[]{new Object[]{"this", Restrictions.eq("idUser", idUser)}});
        List<User> listaUsers = userCon.getLista();
        if (Verify.nullsOrEmpty(listaUsers)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.status(HttpStatus.OK).body(listaUsers);

    }
    

    @GetMapping
    public ResponseEntity getAllUsers() {
        
        userCon.limpaConfiguracoes();
        userCon.setOrderByClause(new OrderBy[]{
            new OrderBy(Order.asc("id"), "this")
        });
        List<User> listaUsers = userCon.getLista();
        if (Verify.nullsOrEmpty(listaUsers)) {
            return ResponseEntity.status(HttpStatus.OK).body(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.status(HttpStatus.OK).body(listaUsers);

    }

    @DeleteMapping("/{idUser}")
    public ResponseEntity deleteUser(@PathVariable Integer idUser) {

        if (userCon.delete(idUser)) {
            return ResponseEntity.status(HttpStatus.OK).body(HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping("/{idUser}")
    public ResponseEntity updateUser(@PathVariable Integer idUser, @Valid @RequestBody User user) {
        user.setId(idUser);
        if (userCon.save(user)) {
            return ResponseEntity.status(HttpStatus.OK).body(HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(HttpStatus.BAD_REQUEST);
        }

    }
    public User getUserByUsername(String username) {
        userCon.limpaConfiguracoes();
        userCon.setCriterions(new Object[]{new Object[]{"this", Restrictions.eq("username", username)}});
        return userCon.getUniqueResult();
    }
}
