package com.br.cidraApi.CidraApi.controller;

import com.br.cidraApi.CidraApi.model.Log;
import com.br.cidraApi.CidraApi.model.Logjson;
import com.br.cidraApi.CidraApi.repositorio.LogCon;
import com.br.cidraApi.CidraApi.util.Verify;
import java.util.List;
import javax.validation.Valid;
import org.hibernate.sql.JoinType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Calven
 */
@Controller
@RequestMapping("/logs")
public class LogController {

    private LogCon logCon = new LogCon();

    @GetMapping
    public ResponseEntity<List<Log>> getAllLogs() {

        logCon.limpaConfiguracoes();
        logCon.setJoin(new Object[]{
            new Object[]{"user", JoinType.LEFT_OUTER_JOIN}
        });
        
        List<Log> listaLogs = logCon.getLista();
        if (Verify.nullsOrEmpty(listaLogs)) {
            return ResponseEntity.status(HttpStatus.OK).body(listaLogs);
        }
        return ResponseEntity.status(HttpStatus.OK).body(listaLogs);
        
        

    }

    @PostMapping
    public ResponseEntity createLog(@Valid @RequestBody Logjson logJson) {

        if (logCon.addLog(logJson)) {
            return ResponseEntity.status(HttpStatus.CREATED).body(HttpStatus.CREATED);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(HttpStatus.BAD_REQUEST);
        }

    }

}
