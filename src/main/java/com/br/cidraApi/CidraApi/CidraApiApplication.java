package com.br.cidraApi.CidraApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CidraApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CidraApiApplication.class, args);
    }
}
