package com.br.cidraApi.CidraApi.util;

import java.util.Collection;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Calven
 */
public class Verify {
    
    public static boolean nullsOrEmpty(Object obj){
        if(obj == null)
            return true;
        
        if(obj instanceof String){
            return ((String) obj).trim().isEmpty();
        } else if(obj instanceof Collection){
            return ((Collection)obj).isEmpty();
        } else if(obj instanceof MultipartFile){
            return ((MultipartFile)obj).isEmpty();
        }
        
        return false;
    }
    
    public static boolean equals(Object o1, Object o2){
        boolean null1 = Verify.nullsOrEmpty(o1);
        boolean null2 = Verify.nullsOrEmpty(o2);
        
        if (null1 && null2){
            return true;
        } else if (null2 != null1){
            return false;
        } else {
            return o1.equals(o2);
        }
    }
}
