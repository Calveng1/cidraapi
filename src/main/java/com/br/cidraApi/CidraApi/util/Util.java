package com.br.cidraApi.CidraApi.util;

import com.br.cidraApi.CidraApi.connector.ProjectionParc;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.criterion.Projections;

/**
 *
 * @author ademilson
 * @since 02/02/2018
 */
public class Util {
    
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private static SimpleDateFormat timestampFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    private static DecimalFormat df;

    public static String getURLWithContextPath(HttpServletRequest request) {
        
        String porta = "";
        
        if (Verify.equals(request.getScheme(), "https") && !Verify.equals(request.getServerPort(), 443)) {
            porta = ":"  + request.getServerPort();
        } else if (Verify.equals(request.getScheme(), "http") && !Verify.equals(request.getServerPort(), 80)) {
            porta = ":"  + request.getServerPort();
        }
        
        return request.getScheme() + "://" + request.getServerName() + porta + request.getContextPath();
    }
    
    public static String concat(String base, String separador, String incremento) {
        if (!Verify.nullsOrEmpty(base)) {
            return base.concat(Verify.nullsOrEmpty(incremento) ? "" : separador.concat(incremento));
        } else if (incremento != null) {
            return incremento;
        } else {
            return "";
        }
    }
    
    public static String apenasNumeros(String str) {
        return str == null ? null : str.replaceAll("[^0-9]", "");
    }
    
    public static String[] getStringVector(String text, String delim){
        List<String> list = new ArrayList();
        StringTokenizer tokenizer = new StringTokenizer(text, delim, false);
        while (tokenizer.hasMoreElements()) {
            list.add(((String) tokenizer.nextElement()).trim());
        }
        
        return list.toArray(new String[0]);
    }
    
    public static String retiraCaracteresEspeciais(String strTransforma) {
        if (strTransforma == null) {
            return null;
        }

        String strRegex = Normalizer.normalize(strTransforma.trim(), Normalizer.Form.NFD); // TRIM e Retirada da acentuação
        strRegex = strRegex.replaceAll("[\\W]", "");                           // Retira: Algo que não seja letra, dígito ou underscore
        strRegex = strRegex.replaceAll("[_]", "");                             // Retira underscore '_'*/

        return strRegex;
    }
    
    public static Date getLimiteInicial(Date date) {

        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
        gc.clear(GregorianCalendar.MINUTE);
        gc.clear(GregorianCalendar.SECOND);
        gc.clear(GregorianCalendar.MILLISECOND);
        
        return gc.getTime();
    }
    
    public static Date getLimiteFinal(Date date) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        gc.set(GregorianCalendar.HOUR_OF_DAY, 23);
        gc.set(GregorianCalendar.MINUTE, 59);
        gc.set(GregorianCalendar.SECOND, 59);
        gc.set(GregorianCalendar.MILLISECOND, 999);
        
        return gc.getTime();
    }
    
    public static Date addDay(Date date, int add) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        gc.add(GregorianCalendar.DAY_OF_MONTH, add);
        return gc.getTime();
    }
    
    public static Date dateAdd(Date date, int field, int add) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        gc.add(field, add);
        return gc.getTime();
    }
    
    /**
     * Altera algum atributo da data.
     * 
     * @param date
     * @param field exemplo: Calendar.MINUTE
     * @param valor
     * @return 
     */
    public static Date dateSet(Date date, int field, int valor) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        gc.set(field, valor);
        return gc.getTime();
    }
    
    /**
     * Retorna o valor do field requerido para a data informada.
     * @param date
     * @param field exemplo: Calendar.DAY_OF_WEEK
     * @return 
     */
    public static int dateGet(Date date, int field) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        return gc.get(field);
    }
    
    public static Date dateSetTime(Date date, String time) {
        date = dateSet(date, Calendar.HOUR_OF_DAY, Integer.parseInt(time.substring(0, time.indexOf(":"))));
        date = dateSet(date, Calendar.MINUTE, Integer.parseInt(time.substring(time.indexOf(":") + 1)));
        return date;
    }
    
    public static Long dateDiferencaDias(Date dtInicial, Date dtFinal) {
        return (dtFinal.getTime() - dtInicial.getTime() + 3600000L) / 86400000L;
    }
    
    public static boolean dateIsBetween(Date dtComparacao, Date date1, Date date2) {
        if (dtComparacao == null || date1 == null || date2 == null) {
            return false;
        }
        
        //esta entre a data1 ou data2 ?
        if (dtComparacao.after(date1) || dtComparacao.equals(date1))
            if (dtComparacao.before(date2)) 
                return true;
        
        return false;
    }
    
    public static String formatDate(Date d){
        if (d == null)
            return null;
        
        return dateFormat.format(d);
    }
    
    public static String formatTimestamp(Date d){
        if (d == null)
            return null;
        return timestampFormat.format(d);
    }
    
    public static String formatTime(Date d){
        if (d == null)
            return null;
        
        return timeFormat.format(d);
    }
    
    public static int timeToMinute(String time) {
        if (Verify.nullsOrEmpty(time)) {
            return 0;
        }
        
        int hour = Integer.parseInt(time.substring(0, time.indexOf(":")));
        int minute = Integer.parseInt(time.substring(time.indexOf(":") + 1));
        
        return (hour * 60) + minute;
    }
    
    public static String minuteToTime(int t) {
        int hours = t / 60; //since both are ints, you get an int
        int minutes = t % 60;
        return String.format("%d:%02d", hours, minutes);
    }
    
    public static Date parseDate(String date, String format){
        if (date != null){
            try {
                return new SimpleDateFormat(format).parse(date);
            } catch (ParseException ex) {
                Logger.getLogger(Util.class.getName()).log(Level.SEVERE, "Erro ao fazer o parse data.", ex);
            }
        }
        
        return null;
    }
    
    public static String formatDate(Date date, String format){
        if (date != null){
            return new SimpleDateFormat(format).format(date);
        }
        
        return null;
    }
    
    public static String formatNumber(Number nbr) {
        if (df == null) {
            df = new DecimalFormat();
            df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(new Locale("pt", "BR")));
            df.setGroupingSize(3);
            df.setMaximumFractionDigits(2);
            df.setMinimumFractionDigits(2);
            df.setMinimumIntegerDigits(1);
        }
        
        if (nbr == null) {
            return null;
        }
        
        return df.format(nbr);
    }
    
    public static String preencheString(String strReduzida, int tamanho, char caracter, boolean isAddAntes){
        StringBuilder strBufferReduzida = new StringBuilder( (strReduzida != null) ? strReduzida : "");
        //***Completa com o caracter passado a string até o tamanho indicado
        while(strBufferReduzida.length() < tamanho){
           if(isAddAntes) {
               strBufferReduzida.insert(0, caracter);
           }else
               strBufferReduzida.append(caracter);
        }
        return strBufferReduzida.substring(0,tamanho);
    }
    
    /**
     * Todos os fields da classe em projeciton
     * 
     * @param clazz
     * @return 
     */
    public static List<ProjectionParc> projectionAllFields(Class clazz) {
        List<ProjectionParc> listaProjection = new ArrayList();
        Field[] fields = clazz.getDeclaredFields();
        
        for (Field field : fields) {
            if (field.getAnnotation(JoinColumn.class) == null && field.getAnnotation(Transient.class) == null){
                listaProjection.add(new ProjectionParc(Projections.property(field.getName()).as(field.getName())));
            }
        }
        
        return listaProjection;
    }
    
//    public static String leArquivo(String arquivoDentroDoProjeto) {
//        String htmlEmail = "";
//
//        InputStream resourceAsStream = Util.class.getResourceAsStream(arquivoDentroDoProjeto);
//        BufferedReader ois = new BufferedReader(new InputStreamReader(resourceAsStream, StandardCharsets.UTF_8));
//
//        if (!Verify.nullsOrEmpty(ois)) {
//            try {
//                String linha;
//                while ((linha = ois.readLine()) != null) {
//                    htmlEmail += linha;
//                }
//            } catch (IOException ex) {
//                Logger.getLogger(EmpresaService.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        
//        return htmlEmail;
//    }
}
