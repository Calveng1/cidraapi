package com.br.cidraApi.CidraApi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Calven
 */
@Entity(name = "users")
@SequenceGenerator(allocationSize = 1, name = "seq_user", sequenceName = "seq_user")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_user")
    @GenericGenerator(name = "seq_user", strategy = "increment")
    private Integer idUser;

    @Column(length = 100, nullable = false, unique = true)
    private String username;

    @Column(length = 100, nullable = false)
    private String password;

    @Column(length = 1, nullable = false, columnDefinition = "char")
    private String stAtivo;
    
    @Column(length = 1, nullable = false, columnDefinition = "char")
    private String stAdmin;

    public Integer getId() {
        return idUser;
    }

    public void setId(Integer id) {
        this.idUser = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStAtivo() {
        return stAtivo;
    }

    public void setStAtivo(String stAtivo) {
        this.stAtivo = stAtivo;
    }

    public String getStAdmin() {
        return stAdmin;
    }

    public void setStAdmin(String stAdmin) {
        this.stAdmin = stAdmin;
    }
  
}
