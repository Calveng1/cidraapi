package com.br.cidraApi.CidraApi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Calven
 */
@Entity(name = "logs")
@SequenceGenerator(allocationSize = 1, name = "seq_log", sequenceName = "seq_log")
public class Log implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_log")
    @GenericGenerator(name = "seq_log", strategy = "increment")
    private Integer idLog;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idUser", nullable = false)
    private User user;

    @Column(nullable = false)
    private String strLog;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date date;

    public Integer getId() {
        return idLog;
    }

    public void setId(Integer idLog) {
        this.idLog = idLog;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStrLog() {
        return strLog;
    }

    public void setStrLog(String strLog) {
        this.strLog = strLog;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
  
}
