package com.br.cidraApi.CidraApi.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Calven
 */
public class Logjson implements Serializable {


    private Integer idUser;
    
    private String strLog;

    private Date date;

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getStrLog() {
        return strLog;
    }

    public void setStrLog(String strLog) {
        this.strLog = strLog;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
