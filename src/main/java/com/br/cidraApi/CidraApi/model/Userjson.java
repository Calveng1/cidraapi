package com.br.cidraApi.CidraApi.model;

import java.io.Serializable;

/**
 *
 * @author Calven
 */
public class Userjson implements Serializable {

    private String username;
    private String password;
    private String stAtivo;
    private String stAdmin;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStAtivo() {
        return stAtivo;
    }

    public void setStAtivo(String stAtivo) {
        this.stAtivo = stAtivo;
    }

    public String getStAdmin() {
        return stAdmin;
    }

    public void setStAdmin(String stAdmin) {
        this.stAdmin = stAdmin;
    }
    

}
