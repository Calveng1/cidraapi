package com.br.cidraApi.CidraApi.security;

import com.br.cidraApi.CidraApi.model.Userjson;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Calven
 */
public class Encrypter {
    public static void encryptPassword(Userjson userJson) 
        throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(userJson.getPassword().getBytes());
        byte[] digest = md.digest();
        String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
        userJson.setPassword(myHash);
    }
}
