package com.br.cidraApi.CidraApi.repositorio;

import com.br.cidraApi.CidraApi.connector.GenericController;
import com.br.cidraApi.CidraApi.model.User;
import com.br.cidraApi.CidraApi.model.Userjson;

/**
 *
 * @author Calven
 */
public class UserCon extends GenericController<User>{

    public UserCon() {
        super(User.class);
    }
    
    public Boolean addUser(Userjson userJson){
        
        User user = new User();
        user.setUsername(userJson.getUsername());
        user.setPassword(userJson.getPassword());
        user.setStAtivo(userJson.getStAtivo());
        user.setStAdmin(userJson.getStAdmin());
        
        return this.save(user);
    }
    
}
