package com.br.cidraApi.CidraApi.repositorio;

import com.br.cidraApi.CidraApi.connector.GenericController;
import com.br.cidraApi.CidraApi.model.Log;
import com.br.cidraApi.CidraApi.model.Logjson;
import com.br.cidraApi.CidraApi.model.User;

/**
 *
 * @author Calven
 */
public class LogCon extends GenericController<Log>{
    
    private final UserCon userCon = new UserCon();
    
    public LogCon() {
        super(Log.class);
    }
    
    public Boolean addLog(Logjson logJson){
        Integer idUser = logJson.getIdUser();
        User userInformado = userCon.get(idUser);
        
        if(userInformado == null){
            return false;
        }
        Log log = new Log();
        log.setUser(userInformado);
        log.setDate(logJson.getDate());
        log.setStrLog(logJson.getStrLog());
        
        return this.save(log);
    }
}
